package padroesprojeto;

public class Circulo implements IFiguraBiDimensional {

	private int raio;
	
	public Circulo(int raio) { /* Inicializa um circulo com um raio*/
		this.raio = raio;
	}
	public Circulo() { /* Raio padrão vai ser 5*/
		this.raio = 5;
	}
	
	@Override
	public double perimetro() { /* O perimetro são 2 PI vezes o raio*/
		return 2 * Math.PI * raio;
	}

	@Override
	public double area() { /* Area do circulo é Pi vezes area ao quadrado*/
		return Math.PI * Math.pow(raio, 2);
		
	}

}
