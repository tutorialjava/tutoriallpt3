package padroesprojeto;

public class Quadrado implements IFiguraBiDimensional{

	private int lado;
	
	public Quadrado(int valor) { /* Recebe um valor para o lado */
		this.lado = valor;
	}
	@Override
	public double area() { /* Area de um quadrado é o lado ao quadrado*/
		return lado * lado;
	}
	
	@Override
	public String toString() { /* Retorna os lados em string*/
		return "(" + this.lado + ", " + this.lado + ", " + this.lado + ", " + this.lado + ")";
	}
	
	@Override
	public double perimetro() { /* Perimetro é o lado vezes 4 */
		return lado * 4;
	}

}
